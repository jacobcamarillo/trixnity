package net.folivo.trixnity.client.store.repository

import net.folivo.trixnity.client.store.UploadCache

typealias UploadMediaRepository = MinimalStoreRepository<String, UploadCache>