package net.folivo.trixnity.client.store.repository

typealias OlmAccountRepository = MinimalStoreRepository<Long, String>