package net.folivo.trixnity.examples.client.ping

import kotlinx.coroutines.runBlocking

fun main() {
    runBlocking { example() }
}