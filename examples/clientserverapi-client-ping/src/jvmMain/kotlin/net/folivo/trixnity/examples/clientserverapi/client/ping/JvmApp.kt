package net.folivo.trixnity.examples.clientserverapi.client.ping

import kotlinx.coroutines.runBlocking

fun main() {
    runBlocking { example() }
}